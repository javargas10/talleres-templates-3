package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		System.out.println("Ingrese el numero de filas de su juego:");
		int filas = sc.nextInt();
		System.out.println("Ingrese el numero de columnas de su juego:");
		int columnas = sc.nextInt();
		System.out.println("Ingrese el nombre del jugador numero 1:");
		String nombre1 = sc.next();
		System.out.println("Ingrese el nombre del jugador numero 2:");
		String nombre2 = sc.next();
		System.out.println("Ingrese el simbolo del jugador 1");
		String simbolo1 = sc.next();
		System.out.println("Ingrese el simbolo del jugador 2");
		String simbolo2 = sc.next();
		
		//Se crean los jugadores con la informacion anterior
		Jugador jugador1 = new Jugador(nombre1,simbolo1);
		Jugador jugador2 = new Jugador(nombre2,simbolo2);
		
		//Se crea el nuevo juego con la informacion proporcionada
		ArrayList<Jugador> sd = new ArrayList<Jugador>();
		sd.add(jugador1);
		sd.add(jugador2);
		LineaCuatro nuevojuego = new LineaCuatro(sd,filas,columnas);
       //TODO
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
	}
}
